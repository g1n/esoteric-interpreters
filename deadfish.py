code = input()
chars = [0] * len(code)
pos = 0
for i in code:
    if i == "i":
        chars[pos] += 1
    if i == "d":
        chars[pos] -= 1
    if i == "s":
        chars[pos] *= chars[pos]
    if i == "o":
        print(chars[pos])
    if chars[pos] == 256 or chars[pos] == -1:
        chars[pos] = 0
        pos += 1
        continue
   # pos += 1
