code = input()
accumulator = 0
for i in code:
    if i == "H":
        print("Hello, World!")
    if i == "Q":
        print(code)
    if i == "9":
        for i in range(99, 0, -1):
            print(f"{i} bottles of beer on the wall,")
            print(f"{i} bottles of beer.")
            print(f"Take one down, pass it around,")
            print()
        print("No bottles of beer on the wall.")
    if i == "+":
        accumulator += 1
